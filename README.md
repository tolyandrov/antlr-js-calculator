
# Javascript ANTLR4 calculator

Set up env variables:

    export CLASSPATH=".:/usr/local/lib/antlr-4.5.3-complete.jar:$CLASSPATH"
    alias antlr4='java -jar /usr/local/lib/antlr-4.5.3-complete.jar'
    alias grun='java org.antlr.v4.gui.TestRig'

## JavaScript Listener

Install antlr4 JavaScript runtime (requires nodejs and npm)

    npm install antlr4

Generate code

    antlr4 -Dlanguage=JavaScript Calculator.g4

Run it

    echo "((1+2) * 2 + 2)/( 1 + 1 )" | node Main.js
