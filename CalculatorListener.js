// Generated from Calculator.g4 by ANTLR 4.7.1
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by CalculatorParser.
function CalculatorListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

CalculatorListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
CalculatorListener.prototype.constructor = CalculatorListener;

// Enter a parse tree produced by CalculatorParser#MulDiv.
CalculatorListener.prototype.enterMulDiv = function(ctx) {
};

// Exit a parse tree produced by CalculatorParser#MulDiv.
CalculatorListener.prototype.exitMulDiv = function(ctx) {
};


// Enter a parse tree produced by CalculatorParser#AddSub.
CalculatorListener.prototype.enterAddSub = function(ctx) {
};

// Exit a parse tree produced by CalculatorParser#AddSub.
CalculatorListener.prototype.exitAddSub = function(ctx) {
};


// Enter a parse tree produced by CalculatorParser#Parens.
CalculatorListener.prototype.enterParens = function(ctx) {
};

// Exit a parse tree produced by CalculatorParser#Parens.
CalculatorListener.prototype.exitParens = function(ctx) {
};


// Enter a parse tree produced by CalculatorParser#Int.
CalculatorListener.prototype.enterInt = function(ctx) {
};

// Exit a parse tree produced by CalculatorParser#Int.
CalculatorListener.prototype.exitInt = function(ctx) {
};



exports.CalculatorListener = CalculatorListener;