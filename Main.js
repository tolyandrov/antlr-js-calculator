"use strict";

let antlr4 = require('antlr4/index.js');
let CalculatorLexer = require("./CalculatorLexer.js");
let CalculatorParser = require("./CalculatorParser.js");
let CalculatorListener = require("./CalculatorListener.js");

// use create for inheritance, see https://developer.mozilla.org/en/docs/Web/JavaScript/Inheritance_and_the_prototype_chain
let Listener = function () {
    CalculatorListener.CalculatorListener.call(this); // chain the constructor
};
// chaining the prototypes
Listener.prototype = Object.create(CalculatorListener.CalculatorListener.prototype);

// override default listener behavior
Listener.prototype.stack = [];
Listener.prototype.exitInt = function (ctx) {
    // stack stores integers only
    this.stack.push(parseInt(ctx.INT().getText()));
};
Listener.prototype.exitMulDiv = function (ctx) {
    let right = this.stack.pop();
    let left = this.stack.pop();
    let result;
    if (ctx.op.type === CalculatorParser.CalculatorParser.MUL) {
        result = left * right;
    } else {
        result = left / right;
    }
    this.stack.push(result);
};
Listener.prototype.exitAddSub = function (ctx) {
    let right = this.stack.pop();
    let left = this.stack.pop();
    let result;
    if (ctx.op.type == CalculatorParser.CalculatorParser.ADD) {
        result = left + right;
    } else {
        result = left - right;
    }
    this.stack.push(result);
};

let main;
main = function (input) {
    let chars = new antlr4.InputStream(input);
    let lexer = new CalculatorLexer.CalculatorLexer(chars);
    let tokens = new antlr4.CommonTokenStream(lexer);
    let parser = new CalculatorParser.CalculatorParser(tokens);
    parser.buildParseTrees = true;
    let tree = parser.expr();

    let printer = new Listener();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(printer, tree);
    console.log(printer.stack.slice(-1).pop());
};

process.stdin.setEncoding('utf8');
let s = '';
process.stdin.on('readable', () => {
    let chunk = process.stdin.read();
    if (chunk !== null) {
        s += chunk;
    }
});
process.stdin.on('end', () => {
    main(s);
});

